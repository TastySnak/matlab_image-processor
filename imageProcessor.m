function varargout = imageProcessor(varargin)
% IMAGEPROCESSOR MATLAB code for imageProcessor.fig
%      IMAGEPROCESSOR, by itself, creates a new IMAGEPROCESSOR or raises the existing
%      singleton*.
%
%      H = IMAGEPROCESSOR returns the handle to a new IMAGEPROCESSOR or the handle to
%      the existing singleton*.
%
%      IMAGEPROCESSOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMAGEPROCESSOR.M with the given input arguments.
%
%      IMAGEPROCESSOR('Property','Value',...) creates a new IMAGEPROCESSOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before imageProcessor_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to imageProcessor_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help imageProcessor

% Last Modified by GUIDE v2.5 06-Dec-2018 16:30:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @imageProcessor_OpeningFcn, ...
                   'gui_OutputFcn',  @imageProcessor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before imageProcessor is made visible.
function imageProcessor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to imageProcessor (see VARARGIN)

% Choose default command line output for imageProcessor
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes imageProcessor wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Global variables
global image;
global current_filter;  % 1: Original | 2: Grey | 3: Black & White


% --- Outputs from this function are returned to the command line.
function varargout = imageProcessor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load.
function load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global image;
global current_filter;

[filename pathname] = uigetfile({'*.jpg'}, 'File Selector');
fullpathname = strcat(pathname, filename);

axes(handles.axes1);
image = imread(fullpathname);
imshow(image);

set(handles.slider, 'Value', 1);
current_filter = 1;

% --- Executes on button press in blackwhite.
function blackwhite_Callback(hObject, eventdata, handles)
% hObject    handle to blackwhite (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global image;
global current_filter;

axes(handles.axes1);
bw = im2bw(image);
imshow(bw);

set(handles.slider, 'Value', 1);
current_filter = 3;

% --- Executes on button press in grey.
function grey_Callback(hObject, eventdata, handles)
% hObject    handle to grey (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global image;
global current_filter;

axes(handles.axes1);
grey = rgb2gray(image);
imshow(grey);

set(handles.slider, 'Value', 1);
current_filter = 2;

% --- Executes on slider movement.
function slider_Callback(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global image;
global current_filter;

switch current_filter
    case 1
        val = get(handles.slider, 'Value');
        brightness = val * image;
    case 2
        grey = rgb2gray(image);
        val = get(handles.slider, 'Value');
        brightness = val * grey;
    case 3
        bw = im2bw(image);
        val = get(handles.slider, 'Value');
        brightness = val * bw;
end

axes(handles.axes1);
imshow(brightness);

% --- Executes during object creation, after setting all properties.
function slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in original.
function original_Callback(hObject, eventdata, handles)
% hObject    handle to original (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global image;
global current_filter;

axes(handles.axes1);
imshow(image);

set(handles.slider, 'Value', 1);
current_filter = 1;
